function HomePage() {

  return (


    <main>
      <div className="p-3 mb-2">
        <title>JamPack'd</title>
        <h1 className="JP_title borderstyle mb-0">JamPack'd</h1>
        <h2 className="JP_subtitle borderstyle">Find your new favorite tune today!</h2>
        <img src="https://i.imgur.com/HxIEBd3.png" className="JP_image" alt="JamPackd Logo"></img>

      </div>
    </main >
  );
}
export default HomePage;


